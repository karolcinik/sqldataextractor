import mysql.connector
import pickle

database = 'smartassistant'

connection = mysql.connector.connect(host='localhost',
                                     port=3310,
                                     database=database,
                                     user='zoovuadmin2',
                                     password='f30okkpog40k=;m;ok3110i=32$_')

# query = "SELECT * FROM customers WHERE customerNumber = 157"

fk_query_referenced = """
SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_NAME = %s;
"""

fk_query_named = """
SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  TABLE_NAME = %s AND
  REFERENCED_TABLE_NAME IS NOT NULL;
"""

select_row = "SELECT * FROM {} WHERE {} = {}"

global_rows = []
table_name = []
global_inserts = []


def get_fks_referenced(table_schema_name):
    cursor = connection.cursor()
    cursor.execute(fk_query_referenced, table_schema_name)
    rows = []
    for row in cursor:
        if row[0] not in table_name:
            table_name.append(row[0])
        if row[3] not in table_name:
            table_name.append(row[3])
        if row not in global_rows:
            global_rows.append(row)
            rows.append(row)
    cursor.close()
    return rows


def get_fks_named(table_schema_name):
    cursor = connection.cursor()
    cursor.execute(fk_query_named, table_schema_name)
    rows = []
    for row in cursor:
        if row[0] not in table_name:
            table_name.append(row[0])
        if row[3] not in table_name:
            table_name.append(row[3])
        if row not in global_rows:
            global_rows.append(row)
            rows.append(row)
    cursor.close()
    return rows


def parse(table_schema_name, input):
    if input is not None:
        schema_name = (input[0],)
        for row in get_fks_referenced(schema_name):
            print(row)
            parse(None, row)
    else:
        for row in get_fks_referenced(table_schema_name):
            parse(None, row)


def parse_origin(table_schema_name, input):
    if input is not None:
        schema_name = (input[3],)
        for row in get_fks_named(schema_name):
            print(row)
            parse_origin(None, row)
    else:
        for row in get_fks_named(table_schema_name):
            parse_origin(None, row)


def get_table_vals(table_name, column_name, id):
    query = select_row.format(table_name, column_name, id)
    cursor = connection.cursor(buffered=True)
    cursor.execute(query)
    values = cursor.fetchone()
    if values is None:
        cursor.close()
        return None
    vals = []
    for v in values:
        if v is None:
            v = 'NULL'
        elif isinstance(v, (bytes, bytearray)):
            v = str(v, "utf-8")
        else:
            v = str(v)
        vals.append(v)
    rows = dict(zip(cursor.column_names, vals))
    cursor.close()
    return rows


def get_table_by_name(name, all_rows):
    rows = []
    for row in all_rows:
        if name == row[0]:
            rows.append(row)
    return rows


def get_table_reference_by_name(name, all_rows):
    rows = []
    for row in all_rows:
        if name == row[3]:
            rows.append(row)
    return rows


def convert_to_sql(table, rows):
    if rows is None:
        return
    columns = ','.join(rows.keys())
    values = []
    for k in rows:
        v = rows[k]
        if v is None:
            v = None
        elif isinstance(v, (bytes, bytearray)):
            v = '\'' + str(v, "utf-8") + '\''
        elif isinstance(v, str):
            v = '\'' + v + '\''
        else:
            v = str(v)
        values.append(v)
    vals = ','.join(values)
    return "INSERT INTO {} ({}) VALUES ({});".format(table, columns, vals)


def get_next_table(rows, fk_desc):
    for fk in fk_desc:
        if rows[fk[1]] is not None:
            key_val = rows[fk[1]]
            table_name = fk[3]
            column_name = fk[4]
            if rows not in global_inserts:
                global_inserts.append(rows)
                print(convert_to_sql(fk[0], rows))
                # print('table name:', fk[0], rows)
                fks = get_table_by_name(table_name, global_rows)
                r = get_table_vals(table_name, column_name, key_val)
                get_next_table(r, fks);

def get_prev_table(rows, fk_desc):
    for fk in fk_desc:
        if rows[fk[1]] is not None:
            key_val = rows[fk[1]]
            table_name = fk[3]
            column_name = fk[4]
            print('table name:', fk[0], rows)
            if rows not in global_inserts:
                global_inserts.append(rows)
                print(convert_to_sql(fk[0], rows))
                fks = get_table_by_name(table_name, global_rows)
                r = get_table_vals(table_name, column_name, key_val)
                get_prev_table(r, fks);


if __name__ == '__main__':
    table_name_file = 'table_name.data'
    global_rows_file = 'global_rows.data'
    # table_schema_name = ('advisor',)
    # parse(table_schema_name, None)
    #
    # for row in table_name:
    #     parse_origin((row,), None)
    #
    # afile = open(r'table_name.data', 'wb')
    # pickle.dump(table_name, afile)
    # afile.close()
    #
    # afile = open(r'global_rows.data', 'wb')
    # pickle.dump(global_rows, afile)
    # afile.close()

    file = open(table_name_file, 'rb')
    table_names_list = pickle.load(file)
    file.close()

    file = open(global_rows_file, 'rb')
    global_rows = pickle.load(file)
    file.close()

    print(table_name)
    for row in global_rows:
        print(row)

    table = 'localization'
    column = 'id'
    id = '2'

    # fk_desc = get_table_by_name(table, global_rows)
    # rows = get_table_vals(table, 'id', 83765)
    # get_next_table(rows, fk_desc)

#<class 'tuple'>: ('advisor', 'live_revision', 'fk_advisor_live_revision', 'advisor_revision', 'id')
    fk_desc = get_table_reference_by_name(table, global_rows)
    source_rows = get_table_vals(table, column, id)
    for fk in fk_desc:
        tab_name = fk[0]
        column = fk[1]
        ref_key_tab_name = fk[3]
        ref_key_col_name = fk[4]
        if source_rows is not None:
            ref_rows = get_table_vals(tab_name, column, source_rows[ref_key_col_name])
            # print(ref_rows)
            if ref_rows is not None:
                print(convert_to_sql(tab_name, ref_rows))

    # table = 'advisor_code'
    # column = 'code'
    # id = '\'RwJ1SJZG\'
    # fk_desc = get_table_by_name(table, global_rows)
    # source_rows = get_table_vals(table, column, id)
    # for fk in fk_desc:
    #     tab_name = fk[0]
    #     column = fk[1]
    #     ref_key_tab_name = fk[3]
    #     ref_key_col_name = fk[4]
    #     if source_rows is not None:
    #         ref_rows = get_table_vals(ref_key_tab_name, ref_key_col_name, source_rows[column])
    #         # print(ref_rows)
    #         if ref_rows is not None:
    #             print(convert_to_sql(ref_key_tab_name, ref_rows))


    # rows = get_table_insert(table, 'id', 30870)
    # get_prev_table(rows, fk_desc)

    connection.close()
